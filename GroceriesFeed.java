package henrygroceries;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.TemporalUnit;

public class GroceriesFeed {
	
	private float soup;
	private float bread;
	private float milk;
	private float apple;
	
	public static final String SOUP = "soup";
	public static final String BREAD = "bread";
	public static final String MILK = "milk";
	public static final String APPLE = "apple";
	
	public GroceriesFeed() {
		
		this.soup = 0.65f;
		this.bread = 0.80f;
		this.milk = 1.30f;
		this.apple = 0.10f;
		
	}
	
	float getSoupPrice() {
		return this.soup;
	}
	
	float getBreadPrice() {
		return this.bread;
	}
	
	float getMilkPrice() {
		return this.milk;
	}
	
	float getApplePrice() {
		return this.apple;
	}
	
	float getActualPrice(String item) {
		
		switch(item) {
		
		case BREAD : return this.bread;
		case APPLE : return this.apple;
		case SOUP : return this.soup;
		case MILK : return this.milk;
		default : return 0f;
		
 		}
		
	}
	
	float getOfferPrice(String item) {
		
		switch(item) {
		
		case BREAD : return this.bread / 2;
		case APPLE : return this.apple * 0.9f;
		case SOUP : return this.soup;
		case MILK : return this.milk;
		default : return 0f;
		
 		}
	}
	
	float getTotalPrice(int numOfSoupTins, int numOfBreadLoaves, int numOfMilkBottles, int numOfApples, LocalDate purchaseDate) {
		
		System.out.println("Soup total cost : " + getSoupTotal(numOfSoupTins, purchaseDate));
		System.out.println("Bread total cost : " + getBreadTotal(numOfBreadLoaves, numOfSoupTins, purchaseDate) + (isOfferApplicable(BREAD, purchaseDate) ? " (offer)" : ""));
		System.out.println("Milk total cost : " + getMilkTotal(numOfMilkBottles, purchaseDate));
		System.out.println("Apple total cost : " + getAppleTotal(numOfApples, purchaseDate) + (isOfferApplicable(APPLE, purchaseDate) ? " (offer)" : ""));
		
		return getSoupTotal(numOfSoupTins, purchaseDate)
				+ getBreadTotal(numOfBreadLoaves, numOfSoupTins, purchaseDate)
				+ getMilkTotal(numOfMilkBottles, purchaseDate)
				+ getAppleTotal(numOfApples, purchaseDate);
	}
	
	float getSoupTotal(int numOfSoupTins, LocalDate purchaseDate) {
		return numOfSoupTins * getActualPrice(SOUP);
	}
	
	float getBreadTotal(int numOfBreadLoaves, int numOfSoupTins, LocalDate purchaseDate) {
		
		if(isOfferApplicable(BREAD, purchaseDate)) {
			int offerAppliedLoafs = (numOfSoupTins / 2) > 0 ? numOfBreadLoaves > 1 ? numOfBreadLoaves - (numOfSoupTins / 2) : numOfBreadLoaves : 0;

			
			return offerAppliedLoafs * getOfferPrice(BREAD) + (numOfBreadLoaves - offerAppliedLoafs) * getActualPrice(BREAD);
		}
		
		return  numOfBreadLoaves * getActualPrice(BREAD);
	}
	
	float getMilkTotal(int numOfMilkBottles, LocalDate purchaseDate) {
		return numOfMilkBottles * getActualPrice(MILK);
	}
	
	float getAppleTotal(int numOfApples, LocalDate purchaseDate) {
		return numOfApples * (isOfferApplicable(APPLE, purchaseDate) ? getOfferPrice(APPLE) : getActualPrice(APPLE));
	}
	
	boolean isOfferApplicable(String item, LocalDate purchaseDate) {
		
		LocalDate currentDate = LocalDate.now();
		
		switch(item) {
		case BREAD : // from yesterday - for 7 days
			if(purchaseDate.isBefore(currentDate.minusDays(1)) || purchaseDate.isAfter(currentDate.plusDays(5)))  {
				return false;
			}
			return true;
			
		case APPLE : // from 3 days hence - until the end of the following month
			if(purchaseDate.isBefore(currentDate.plusDays(3)) || 
					purchaseDate.isAfter(currentDate.plusMonths(1).with(TemporalAdjusters.lastDayOfMonth()))) {
				return false;
			}
			return true;
		default :
			return false;
		}
	}

}
