package henrygroceries;

import java.time.LocalDate;
import java.util.Scanner;

public class HenryGroceries {
	
	static Scanner scanner;
	static GroceriesFeed groceries;
	
	public static void main(String[] args) {
				
		System.out.println("Welcome to Henry's Store");
		scanner = new Scanner(System.in);
		
		System.out.println("Enter the number of soup tins :");
		int numOfSoupTins = scanner.nextInt();
		System.out.println("Enter the number of bread loaves :");
		int numOfBreadLoaves = scanner.nextInt();
		System.out.println("Enter the number of milk bottles :");
		int numOfMilkBottles = scanner.nextInt();
		System.out.println("Enter the number of apples :");
		int numOfApples = scanner.nextInt();
		System.out.println("Enter the date of purchase (YYYY-MM-DD) :");
		String dateInput = scanner.next();
		
		LocalDate purchaseDate = LocalDate.parse(dateInput);
		
		groceries = new GroceriesFeed();
		
		float totalPrice = groceries.getTotalPrice(numOfSoupTins, numOfBreadLoaves, numOfMilkBottles, numOfApples, purchaseDate);
		
		System.out.println("Total price is : " + totalPrice);
		
	}

}
